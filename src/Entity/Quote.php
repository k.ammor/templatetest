<?php

namespace Evaneos\Entity;

use Evaneos\Helper\NotNullTrait;

class Quote
{
    use NotNullTrait;

    /** @var int */
    private $id;
    /** @var int */
    private $siteId;
    /** @var int */
    private $destinationId;
    /** @var \DateTime */
    private $dateQuoted;

    /**
     * @param int $id
     * @param int $siteId
     * @param int $destinationId
     * @param \DateTime $dateQuoted
     */
    public function __construct($id, $siteId, $destinationId, \DateTime $dateQuoted)
    {
        $this->id = $id;
        $this->siteId = $siteId;
        $this->destinationId = $destinationId;
        $this->dateQuoted = $dateQuoted;
    }

    public function renderHtml()
    {
        return '<p>' . $this->getId() . '</p>';
    }

    public function renderText()
    {
        return (string) $this->getId();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getSiteId()
    {
        return $this->siteId;
    }

    /**
     * @return int
     */
    public function getDestinationId()
    {
        return $this->destinationId;
    }
}