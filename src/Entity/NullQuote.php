<?php

namespace Evaneos\Entity;

use Evaneos\Helper\NullTrait;

class NullQuote extends Quote
{
    use NullTrait;

    public function __construct()
    {
    }
}