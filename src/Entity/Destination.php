<?php

namespace Evaneos\Entity;

class Destination
{
    /** @var int */
    private $id;
    /** @var string */
    private $countryName;
    /** @var string */
    private $conjunction;
    /** @var string */
    private $computerName;

    /**
     * @param string $id
     * @param string $countryName
     * @param string $conjunction
     * @param string $computerName
     */
    public function __construct($id, $countryName, $conjunction, $computerName)
    {
        $this->id = $id;
        $this->countryName = $countryName;
        $this->conjunction = $conjunction;
        $this->computerName = $computerName;
    }

    /**
     * @return string
     */
    public function getCountryName()
    {
        return $this->countryName;
    }
}
