<?php

namespace Evaneos\Entity;

class Template
{
    /** @var int */
    private $id;
    /** @var string */
    private $subject;
    /** @var string */
    private $content;

    /**
     * @param int $id
     * @param string $subject
     * @param string $content
     */
    public function __construct($id, $subject, $content)
    {
        $this->id = $id;
        $this->subject = $subject;
        $this->content = $content;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
}