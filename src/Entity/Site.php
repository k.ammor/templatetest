<?php

namespace Evaneos\Entity;

class Site
{
    /** @var int */
    private $id;
    /** @var string */
    private $url;

    /**
     * @param int $id
     * @param string $url
     */
    public function __construct($id, $url)
    {
        $this->id = $id;
        $this->url = $url;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }
}
