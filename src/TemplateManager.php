<?php

namespace Evaneos;

use Evaneos\Entity\Destination;
use Evaneos\Entity\NullQuote;
use Evaneos\Entity\Quote;
use Evaneos\Entity\Template;
use Evaneos\Entity\User;
use Evaneos\Repository\DestinationRepositoryInterface;
use Evaneos\Repository\QuoteRepositoryInterface;
use Evaneos\Repository\SiteRepositoryInterface;

class TemplateManager
{
    /** @var User */
    private $currentUser;
    /** @var QuoteRepositoryInterface */
    private $quoteRepository;
    /** @var DestinationRepositoryInterface */
    private $destinationRepository;
    /** @var SiteRepositoryInterface */
    private $siteRepository;

    /**
     * @param User $currentUser
     * @param QuoteRepositoryInterface $quoteRepository
     * @param DestinationRepositoryInterface $destinationRepository
     * @param SiteRepositoryInterface $siteRepository
     */
    public function __construct(
        User $currentUser,
        QuoteRepositoryInterface $quoteRepository,
        DestinationRepositoryInterface $destinationRepository,
        SiteRepositoryInterface $siteRepository
    ) {
        $this->currentUser = $currentUser;
        $this->quoteRepository = $quoteRepository;
        $this->destinationRepository = $destinationRepository;
        $this->siteRepository = $siteRepository;
    }

    /**
     * @param Template $templateToCompute
     * @param array $data
     * @return Template
     */
    public function getTemplateComputed(Template $templateToCompute, array $data)
    {
        $quote = $this->getQuoteFromData($data);
        $user = (isset($data['user']) && $data['user'] instanceof User) ? $data['user'] : $this->currentUser;

        return new Template(
            $templateToCompute->getId(),
            $this->computeText($templateToCompute->getSubject(), $user, $quote),
            $this->computeText($templateToCompute->getContent(), $user, $quote)
        );
    }

    /**
     * @param string $text
     * @param User $user
     * @param Quote $quote
     * @return string
     */
    private function computeText($text, User $user, Quote $quote)
    {
        $replacementTokenList = [];
        $destinationLink = '';
        if (!$quote->isNull()) {
            $destination = $this->destinationRepository->getById($quote->getDestinationId());

            $replacementTokenList['[quote:summary_html]'] = $quote->renderHtml();
            $replacementTokenList['[quote:summary]'] = $quote->renderText();
            $replacementTokenList['[quote:destination_name]'] = $destination->getCountryName();

            if (strpos($text, '[quote:destination_link]') !== false) {
                $destinationLink = $this->getDestinationLink($quote, $destination);
            }
        }

        $replacementTokenList['[quote:destination_link]'] = $destinationLink;
        $replacementTokenList['[user:first_name]'] = ucfirst(mb_strtolower($user->getFirstName()));

        return str_replace(array_keys($replacementTokenList), array_values($replacementTokenList), $text);
    }

    /**
     * @param Quote $quote
     * @param Destination $destination
     * @return string
     */
    private function getDestinationLink(Quote $quote, Destination $destination)
    {
        $site = $this->siteRepository->getById($quote->getSiteId());

        return $site->getUrl() . '/' . $destination->getCountryName() . '/quote/' . $quote->getId();
    }

    /**
     * @param array $data
     * @return Quote
     */
    private function getQuoteFromData(array $data)
    {
        return (isset($data['quote']) && $data['quote'] instanceof Quote) ? $this->quoteRepository->getById($data['quote']->getId()) : new NullQuote();
    }
}
