<?php

namespace Evaneos\Repository;

use Evaneos\Entity\Site;

interface SiteRepositoryInterface
{
    /**
     * @param int $id
     *
     * @return Site
     */
    public function getById($id);
}