<?php

namespace Evaneos\Repository;

use Evaneos\Entity\Quote;

interface QuoteRepositoryInterface
{
    /**
     * @param int $id
     *
     * @return Quote
     */
    public function getById($id);
}