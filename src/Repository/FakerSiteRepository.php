<?php

namespace Evaneos\Repository;

use Evaneos\Entity\Site;

class FakerSiteRepository implements SiteRepositoryInterface
{
    /** @var string */
    private $url;

    /**
     * SiteRepository constructor.
     */
    public function __construct()
    {
        // DO NOT MODIFY THIS METHOD
        $this->url = \Faker\Factory::create()->url;
    }

    /**
     * @param int $id
     *
     * @return Site
     */
    public function getById($id)
    {
        // DO NOT MODIFY THIS METHOD
        return new Site($id, $this->url);
    }
}
