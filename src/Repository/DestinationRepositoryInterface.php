<?php

namespace Evaneos\Repository;

use Evaneos\Entity\Destination;

interface DestinationRepositoryInterface
{
    /**
     * @param int $id
     *
     * @return Destination
     */
    public function getById($id);
}