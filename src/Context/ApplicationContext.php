<?php

namespace Evaneos\Context;

use Evaneos\Entity\Site;
use Evaneos\Entity\User;
use Evaneos\Helper\SingletonTrait;
use Evaneos\Repository\FakerDestinationRepository;
use Evaneos\Repository\DestinationRepositoryInterface;
use Evaneos\Repository\FakerQuoteRepository;
use Evaneos\Repository\QuoteRepositoryInterface;
use Evaneos\Repository\FakerSiteRepository;
use Evaneos\Repository\SiteRepositoryInterface;
use Evaneos\TemplateManager;

class ApplicationContext
{
    use SingletonTrait;

    /** @var Site */
    private $currentSite;
    /** @var User */
    private $currentUser;
    /** @var QuoteRepositoryInterface */
    private $quoteRepository;
    /** @var DestinationRepositoryInterface */
    private $destinationRepository;
    /** @var SiteRepositoryInterface */
    private $siteRepository;
    /** @var TemplateManager */
    private $templateManager;

    protected function __construct()
    {
        $faker = \Faker\Factory::create();
        $this->currentSite = new Site($faker->randomNumber(), $faker->url);
        $this->currentUser = new User($faker->randomNumber(), $faker->firstName, $faker->lastName, $faker->email);
    }

    /** @return Site */
    public function getCurrentSite()
    {
        return $this->currentSite;
    }

    /** @return User */
    public function getCurrentUser()
    {
        return $this->currentUser;
    }

    /** @return QuoteRepositoryInterface */
    public function getQuoteRepository()
    {
        if (!$this->quoteRepository) {
            $this->quoteRepository = new FakerQuoteRepository();
        }

        return $this->quoteRepository;
    }

    /** @return DestinationRepositoryInterface */
    public function getDestinationRepository()
    {
        if (!$this->destinationRepository) {
            $this->destinationRepository = new FakerDestinationRepository();
        }

        return $this->destinationRepository;
    }

    /** @return SiteRepositoryInterface */
    public function getSiteRepository()
    {
        if (!$this->siteRepository) {
            $this->siteRepository = new FakerSiteRepository();
        }

        return $this->siteRepository;
    }

    /** @return TemplateManager */
    public function getTemplateManager()
    {
        if (!$this->templateManager) {
            $this->templateManager = new TemplateManager(
                $this->getCurrentUser(),
                $this->getQuoteRepository(),
                $this->getDestinationRepository(),
                $this->getSiteRepository()
            );
        }

        return $this->templateManager;
    }
}
