<?php

namespace Evaneos\Helper;

trait NotNullTrait
{
    public function isNull()
    {
        return false;
    }
}