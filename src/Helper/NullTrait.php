<?php

namespace Evaneos\Helper;

trait NullTrait
{
    public function isNull()
    {
        return true;
    }
}