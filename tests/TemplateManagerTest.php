<?php

namespace Evaneos;

use Evaneos\Entity\Destination;
use Evaneos\Entity\Quote;
use Evaneos\Entity\Site;
use Evaneos\Entity\Template;
use Evaneos\Entity\User;
use Evaneos\Repository\DestinationRepositoryInterface;
use Evaneos\Repository\QuoteRepositoryInterface;
use Evaneos\Repository\SiteRepositoryInterface;
use Prophecy\Argument;

class TemplateManagerTest extends \PHPUnit_Framework_TestCase
{
    /** @var \Faker\Generator */
    private $faker;
    /** @var User */
    private $currentUser;
    /** @var TemplateManager */
    private $templateManager;
    /** @var QuoteRepositoryInterface */
    private $quoteRepository;
    /** @var DestinationRepositoryInterface */
    private $destinationRepository;
    /** @var SiteRepositoryInterface */
    private $siteRepository;

    /**
     * Init the mocks
     */
    public function setUp()
    {
        $this->faker = \Faker\Factory::create();
        $this->currentUser = $this->getRandomUser();
        $this->quoteRepository = $this->prophesize(QuoteRepositoryInterface::class);
        $this->destinationRepository = $this->prophesize(DestinationRepositoryInterface::class);
        $this->siteRepository = $this->prophesize(SiteRepositoryInterface::class);

        $this->templateManager = new TemplateManager(
            $this->currentUser,
            $this->quoteRepository->reveal(),
            $this->destinationRepository->reveal(),
            $this->siteRepository->reveal()
        );
    }

    public function testComputeTemplateWithoutQuoteNorUser()
    {
        $template = new Template(
            1,
            'Votre voyage avec une agence locale [quote:destination_name]',
            "
Bonjour [user:first_name],

Merci d'avoir contacté un agent local pour votre voyage [quote:destination_name].
Le lien vers votre destination est le suivant: [quote:destination_link].

Nous vous invitons à lire le résumé suivant pour en savoir plus:
[quote:summary_html]
Si votre navigateur ne lit pas le HTML:
[quote:summary]

Bien cordialement,

L'équipe Evaneos.com
www.evaneos.com
");
        $expectedComputedTemplate = new Template(
            1,
            'Votre voyage avec une agence locale [quote:destination_name]',
            '
Bonjour ' . ucfirst(mb_strtolower($this->currentUser->getFirstName())) . ",

Merci d'avoir contacté un agent local pour votre voyage [quote:destination_name].
Le lien vers votre destination est le suivant: .

Nous vous invitons à lire le résumé suivant pour en savoir plus:
[quote:summary_html]
Si votre navigateur ne lit pas le HTML:
[quote:summary]

Bien cordialement,

L'équipe Evaneos.com
www.evaneos.com
");

        $message = $this->templateManager->getTemplateComputed($template, []);

        $this->assertEquals($expectedComputedTemplate, $message);
    }

    public function testComputeTemplateWithQuoteWithoutUser()
    {
        $destination = $this->getRandomDestination();
        $quote = $this->getRandomQuote();
        $site = $this->getRandomSite();

        $template = new Template(
            1,
            'Votre voyage avec une agence locale [quote:destination_name]',
            "
Bonjour [user:first_name],

Merci d'avoir contacté un agent local pour votre voyage [quote:destination_name].
Le lien vers votre destination est le suivant: [quote:destination_link].

Nous vous invitons à lire le résumé suivant pour en savoir plus:
[quote:summary_html]
Si votre navigateur ne lit pas le HTML:
[quote:summary]

Bien cordialement,

L'équipe Evaneos.com
www.evaneos.com
");
        $expectedComputedTemplate = new Template(
            1,
            'Votre voyage avec une agence locale ' . $destination->getCountryName(),
            '
Bonjour ' . ucfirst(mb_strtolower($this->currentUser->getFirstName())) . ",

Merci d'avoir contacté un agent local pour votre voyage " . $destination->getCountryName() . '.
Le lien vers votre destination est le suivant: ' . $this->getDestinationLink($site->getUrl(), $quote->getId(), $destination->getCountryName()) . '.

Nous vous invitons à lire le résumé suivant pour en savoir plus:
' . $quote->renderHtml() . '
Si votre navigateur ne lit pas le HTML:
' . $quote->renderText() ."

Bien cordialement,

L'équipe Evaneos.com
www.evaneos.com
"
        );

        $this->quoteRepository->getById($quote->getId())
            ->willReturn($quote);

        $this->destinationRepository->getById($quote->getDestinationId())
            ->willReturn($destination);

        $this->siteRepository->getById($quote->getSiteId())
            ->willReturn($site);

        $message = $this->templateManager->getTemplateComputed(
            $template,
            [
                'quote' => $quote
            ]
        );

        $this->assertEquals($expectedComputedTemplate, $message);
    }

    public function testComputeTemplateWithUserWithoutQuote()
    {
        $user = $this->getRandomUser();

        $template = new Template(
            1,
            'Votre voyage avec une agence locale [quote:destination_name]',
            "
Bonjour [user:first_name],

Merci d'avoir contacté un agent local pour votre voyage [quote:destination_name].
Le lien vers votre destination est le suivant: [quote:destination_link].

Nous vous invitons à lire le résumé suivant pour en savoir plus:
[quote:summary_html]
Si votre navigateur ne lit pas le HTML:
[quote:summary]

Bien cordialement,

L'équipe Evaneos.com
www.evaneos.com
");
        $expectedComputedTemplate = new Template(
            1,
            'Votre voyage avec une agence locale [quote:destination_name]',
            '
Bonjour ' . ucfirst(mb_strtolower($user->getFirstName())) . ",

Merci d'avoir contacté un agent local pour votre voyage [quote:destination_name].
Le lien vers votre destination est le suivant: .

Nous vous invitons à lire le résumé suivant pour en savoir plus:
[quote:summary_html]
Si votre navigateur ne lit pas le HTML:
[quote:summary]

Bien cordialement,

L'équipe Evaneos.com
www.evaneos.com
"
        );


        $message = $this->templateManager->getTemplateComputed(
            $template,
            [
                'user' => $user
            ]
        );

        $this->assertEquals($expectedComputedTemplate, $message);
    }

    public function testComputeTemplateWithQuoteAndUserNothingToReplace()
    {
        $user = $this->getRandomUser();
        $quote = $this->getRandomQuote();

        $template = new Template(
            5,
            'Votre voyage avec une agence locale',
            "
Bonjour,

Merci d'avoir contacté un agent local pour votre voyage.
Nous avons bien pris en compte votre demnade et reviendrons vers vous rapidement avec une proposition personnalisée.

Bien cordialement,

L'équipe Evaneos.com
www.evaneos.com
");
        $expectedComputedTemplate = clone $template;

        $this->quoteRepository->getById(Argument::cetera())
            ->willReturn($this->getRandomQuote());

        $this->destinationRepository->getById(Argument::cetera())
            ->willReturn($this->getRandomDestination());

        $message = $this->templateManager->getTemplateComputed($template, ['quote' => $quote, 'user' => $user]);

        $this->assertEquals($expectedComputedTemplate, $message);
    }

    public function testComputeTemplateWithQuoteAndUser()
    {
        $destination = $this->getRandomDestination();
        $user = $this->getRandomUser();
        $quote = $this->getRandomQuote();
        $site = $this->getRandomSite();

        $template = new Template(
            1,
            'Votre voyage avec une agence locale [quote:destination_name]',
            "
Bonjour [user:first_name],

Merci d'avoir contacté un agent local pour votre voyage [quote:destination_name].
Le lien vers votre destination est le suivant: [quote:destination_link].

Nous vous invitons à lire le résumé suivant pour en savoir plus:
[quote:summary_html]
Si votre navigateur ne lit pas le HTML:
[quote:summary]

Bien cordialement,

L'équipe Evaneos.com
www.evaneos.com
");
        $expectedComputedTemplate = new Template(
            1,
            'Votre voyage avec une agence locale ' . $destination->getCountryName(),
            '
Bonjour ' . ucfirst(mb_strtolower($user->getFirstName())) . ",

Merci d'avoir contacté un agent local pour votre voyage " . $destination->getCountryName() . '.
Le lien vers votre destination est le suivant: ' . $this->getDestinationLink($site->getUrl(), $quote->getId(), $destination->getCountryName()) . '.

Nous vous invitons à lire le résumé suivant pour en savoir plus:
' . $quote->renderHtml() . '
Si votre navigateur ne lit pas le HTML:
' . $quote->renderText() ."

Bien cordialement,

L'équipe Evaneos.com
www.evaneos.com
"
        );

        $this->quoteRepository->getById($quote->getId())
            ->willReturn($quote)
            ->shouldBeCalledTimes(1);

        $this->destinationRepository->getById($quote->getDestinationId())
            ->willReturn($destination)
            ->shouldBeCalledTimes(2);

        $this->siteRepository->getById($quote->getSiteId())
            ->willReturn($site)
            ->shouldBeCalledTimes(1);

        $message = $this->templateManager->getTemplateComputed(
            $template,
            [
                'quote' => $quote,
                'user' => $user
            ]
        );

        $this->assertEquals($expectedComputedTemplate, $message);
    }

    public function testComputeTemplateWithWrongQuoteAndUser()
    {
        $template = new Template(
            1,
            'Votre voyage avec une agence locale [quote:destination_name]',
            "
Bonjour [user:first_name],

Merci d'avoir contacté un agent local pour votre voyage [quote:destination_name].
Le lien vers votre destination est le suivant: [quote:destination_link].

Nous vous invitons à lire le résumé suivant pour en savoir plus:
[quote:summary_html]
Si votre navigateur ne lit pas le HTML:
[quote:summary]

Bien cordialement,

L'équipe Evaneos.com
www.evaneos.com
");
        $expectedComputedTemplate = new Template(
            1,
            'Votre voyage avec une agence locale [quote:destination_name]',
            '
Bonjour ' . ucfirst(mb_strtolower($this->currentUser->getFirstName())) . ",

Merci d'avoir contacté un agent local pour votre voyage [quote:destination_name].
Le lien vers votre destination est le suivant: .

Nous vous invitons à lire le résumé suivant pour en savoir plus:
[quote:summary_html]
Si votre navigateur ne lit pas le HTML:
[quote:summary]

Bien cordialement,

L'équipe Evaneos.com
www.evaneos.com
"
        );

        $this->quoteRepository->getById(Argument::any())
            ->shouldNotBeCalled();

        $this->destinationRepository->getById(Argument::any())
            ->shouldNotBeCalled();

        $this->siteRepository->getById(Argument::any())
            ->shouldNotBeCalled();

        $message = $this->templateManager->getTemplateComputed(
            $template,
            [
                'quote' => 'this is not a quote',
                'user' => 'this is not an user'
            ]
        );

        $this->assertEquals($expectedComputedTemplate, $message);
    }

    /** @return User */
    private function getRandomUser()
    {
        return new User($this->faker->randomNumber(), strtoupper($this->faker->firstName), $this->faker->lastName, $this->faker->email);
    }

    /** @return Quote */
    private function getRandomQuote()
    {
        return new Quote($this->faker->randomNumber(), $this->faker->randomNumber(), $this->faker->randomNumber(), $this->faker->dateTimeBetween());
    }

    /** @return Destination */
    private function getRandomDestination()
    {
        return new Destination($this->faker->randomNumber(), $this->faker->country, 'en', $this->faker->slug());
    }

    private function getRandomSite()
    {
        return new Site($this->faker->randomNumber(), $this->faker->url);
    }

    /**
     * @param string $siteUrl
     * @param string $quoteId
     * @param string $destinationCoutryName
     * @return string
     */
    private function getDestinationLink($siteUrl, $quoteId, $destinationCoutryName)
    {
        return $siteUrl . '/' . $destinationCoutryName . '/quote/' . $quoteId;
    }

}
