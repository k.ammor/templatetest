<?php

use Evaneos\Context\ApplicationContext;
use Evaneos\Entity\Quote;
use Evaneos\Entity\Template;

require_once __DIR__ . '/../vendor/autoload.php';

$faker = \Faker\Factory::create();
$applicationContext = ApplicationContext::getInstance();

$template = new Template(
    1,
    'Votre voyage avec une agence locale [quote:destination_name]',
    "
Bonjour [user:first_name],

Merci d'avoir contacté un agent local pour votre voyage [quote:destination_name].

Bien cordialement,

L'équipe Evaneos.com
www.evaneos.com
");
$templateManager = $applicationContext->getTemplateManager();

$message = $templateManager->getTemplateComputed(
    $template,
    [
        'quote' => new Quote($faker->randomNumber(), $faker->randomNumber(), $faker->randomNumber(), $faker->dateTimeBetween())
    ]
);

echo $message->getSubject() . "\n" . $message->getContent();
